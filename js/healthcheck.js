angular.module('healthCheckApp', []).controller('healthCheckController', function ($timeout, $interval, $http) {
    var healthCheck = this;
    healthCheck.backendApps = {
        // Shared
        shared: [{
            name: "IDM",
            url: "https://xmwise.320.io/shared/idm-app/health-check"
        }, {
            name: "Foundation Data",
            url: "https://xmwise.320.io/shared/fd-app/health-check"
        }, {
            name: "File App",
            url: "https://xmwise.320.io/shared/file-app/health-check"
        }, {
            name: "Push App",
            url: "https://xmwise.320.io/shared/push-app/health-check"
        }, {
            name: "Print App",
            url: "https://xmwise.320.io/shared/print-app/health-check"
        }, {
            name: "WH-Print App",
            url: "https://xmwise.320.io/shared/wh-print-app/health-check"
        }],

        // Viabaron
        viabaron: [{
            name: "WMS",
            url: "https://xmwise.320.io/viabaron/wms-app/health-check"
        }, {
            name: "Base App",
            url: "https://xmwise.320.io/viabaron/base-app/health-check"
        }, {
            name: "BAM",
            url: "https://xmwise.320.io/viabaron/bam/health-check"
        }, {
            name: "Message Handler App",
            url: "https://xmwise.320.io/viabaron/message-handler-app/health-check"
        }, {
            name: "YMS App",
            url: "https://xmwise.320.io/viabaron/yms-app/health-check"
        }],

        // Turnbull
        turnbull: [{
            name: "WMS",
            url: "https://xmwise.320.io/turnbull/wms-app/health-check"
        }, {
            name: "Base App",
            url: "https://xmwise.320.io/turnbull/base-app/health-check"
        }, {
            name: "BAM",
            url: "https://xmwise.320.io/turnbull/bam/health-check"
        }, {
            name: "Message Handler App",
            url: "https://xmwise.320.io/viabaron/message-handler-app/health-check"
        }]
    };

    checkAll();
    $interval(checkAll, 2500);

    function checkAll() {
        healthCheck.backendApps.shared.forEach(check);
        healthCheck.backendApps.viabaron.forEach(check);
        healthCheck.backendApps.turnbull.forEach(check);
    }

    function check(backendApp) {
        backendApp.isFinished = false;
        backendApp.isAlive = false;

        $http({method: "GET", url: backendApp.url, timeout: 5000}).then(function (response) {
            backendApp.isFinished = true;
            backendApp.isAlive = (response.status === 200);

        }, function (response) {
            backendApp.isFinished = true;
            backendApp.isAlive = false;

        });
    }

});